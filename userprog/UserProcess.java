package nachos.userprog;

import nachos.machine.*;
import nachos.threads.*;
import nachos.userprog.*;

import java.io.EOFException;
import java.util.LinkedList;
import java.util.ListIterator;

/**
 * Encapsulates the state of a user process that is not contained in its user
 * thread (or threads). This includes its address translation state, a file
 * table, and information about the program being executed.
 * 
 * <p>
 * This class is extended by other classes to support additional functionality
 * (such as additional syscalls).
 * 
 * @see nachos.vm.VMProcess
 * @see nachos.network.NetProcess
 */
public class UserProcess {
	/**
	 * Allocate a new process.
	 */
	public UserProcess() {
    Lib.debug(dbgProc, "UserProcess.java:UserProcess(): Entering Function");
		// Unique Process Identifier
		ID = nextID++;
		processCount++;
		// Instantiate a new openFileManager
		fileManager = new openFileManager();
	}

	/**
	 * Allocate and return a new process of the correct class. The class name is
	 * specified by the <tt>nachos.conf</tt> key
	 * <tt>Kernel.processClassName</tt>.
	 * 
	 * @return a new process of the correct class.
	 */
	public static UserProcess newUserProcess() {
    Lib.debug(dbgProc, "UserProcess.java:newUserProcess(): Entering Function");
		return (UserProcess) Lib.constructObject(Machine.getProcessClassName());
	}

	/**
	 * Execute the specified program with the specified arguments. Attempts to
	 * load the program, and then forks a thread to run it.
	 * 
	 * @param name the name of the file containing the executable.
	 * @param args the arguments to pass to the executable.
	 * @return <tt>true</tt> if the program was successfully executed.
	 */
	public boolean execute(String name, String[] args) {
    Lib.debug(dbgProc, "UserProcess.java:execute(): Entering Function");
	  //Lib.debug(dbgProcess, " " pages)");
		if (!load(name, args))
			return false;

		// Need to keep track of thread for process
		thread = new UThread(this);
		thread.setName(name).fork();

		return true;
	}

	/**
	 * Save the state of this process in preparation for a context switch.
	 * Called by <tt>UThread.saveState()</tt>.
	 */
	public void saveState() {
    Lib.debug(dbgProc, "UserProcess.java:saveState(): Entering Function");
    Lib.debug(dbgProc, "UserProcess.java:restoreState(): Saving procces with ID: " + ID);
		pageTable = Machine.processor().getPageTable();
    Lib.debug(dbgProc, "UserProcess.java:saveState(): Exiting Function");
	}

	/**
	 * Restore the state of this process after a context switch. Called by
	 * <tt>UThread.restoreState()</tt>.
	 */
	public void restoreState() {
    Lib.debug(dbgProc, "UserProcess.java:restoreState(): Entering Function");
    Lib.debug(dbgProc, "UserProcess.java:restoreState(): Restoring procces with ID: " + ID);
		Machine.processor().setPageTable(pageTable);
    Lib.debug(dbgProc, "UserProcess.java:restoreState(): Exiting Function");
	}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Read a null-terminated string from this process's virtual memory. Read at
	 * most <tt>maxLength + 1</tt> bytes from the specified address, search for
	 * the null terminator, and convert it to a <tt>java.lang.String</tt>,
	 * without including the null terminator. If no null terminator is found,
	 * returns <tt>null</tt>.
	 * 
	 * @param vaddr the starting virtual address of the null-terminated string.
	 * @param maxLength the maximum number of characters in the string, not
	 * including the null terminator.
	 * @return the string read, or <tt>null</tt> if no null terminator was
	 * found.
	 */
	public String readVirtualMemoryString(int vaddr, int maxLength) {
    Lib.debug(dbgProc, "UserProcess.java:readVirtualMemoryString(): Entering Function");
		Lib.assertTrue(maxLength >= 0);

		byte[] bytes = new byte[maxLength + 1];

		int bytesRead = readVirtualMemory(vaddr, bytes, 0, maxLength);

		for (int length = 0; length < bytesRead; length++) {
			if (bytes[length] == 0)
				return new String(bytes, 0, length);
		}

		return null;
	}

	/**
	 * Transfer data from this process's virtual memory to all of the specified
	 * array. Same as <tt>readVirtualMemory(vaddr, data, 0, data.length)</tt>.
	 * 
	 * @param vaddr the first byte of virtual memory to read.
	 * @param data the array where the data will be stored.
	 * @return the number of bytes successfully transferred.
	 */
	public int readVirtualMemory(int vaddr, byte[] data) {
    Lib.debug(dbgProc, "UserProcess.java:readVirtualMemory(): Entering Function");
		return readVirtualMemory(vaddr, data, 0, data.length);
	}

	/**
	 * Transfer data from this process's virtual memory to the specified array.
	 * This method handles address translation details. This method must
	 * <i>not</i> destroy the current process if an error occurs, but instead
	 * should return the number of bytes successfully copied (or zero if no data
	 * could be copied).
	 * 
	 * @param vaddr the first byte of virtual memory to read.
	 * @param data the array where the data will be stored.
	 * @param offset the first byte to write in the array.
	 * @param length the number of bytes to transfer from virtual memory to the
	 * array.
	 * @return the number of bytes successfully transferred.
	 */
	public int readVirtualMemory(int vaddr, byte[] data, int offset, int length) {
    Lib.debug(dbgProc, "UserProcess.java:readVirtualMemory(): Entering Function");
		return accessMemory(false, vaddr, data, offset, length);
	}

	/**
	 * Transfer all data from the specified array to this process's virtual
	 * memory. Same as <tt>writeVirtualMemory(vaddr, data, 0, data.length)</tt>.
	 * 
	 * @param vaddr the first byte of virtual memory to write.
	 * @param data the array containing the data to transfer.
	 * @return the number of bytes successfully transferred.
	 */
	public int writeVirtualMemory(int vaddr, byte[] data) {
    Lib.debug(dbgProc, "UserProcess.java:writeVirtualMemory(): Entering Function");
		return writeVirtualMemory(vaddr, data, 0, data.length);
	}

	/**
	 * Transfer data from the specified array to this process's virtual memory.
	 * This method handles address translation details. This method must
	 * <i>not</i> destroy the current process if an error occurs, but instead
	 * should return the number of bytes successfully copied (or zero if no data
	 * could be copied).
	 * 
	 * @param vaddr the first byte of virtual memory to write.
	 * @param data the array containing the data to transfer.
	 * @param offset the first byte to transfer from the array.
	 * @param length the number of bytes to transfer from the array to virtual
	 * memory.
	 * @return the number of bytes successfully transferred.
	 */
	public int writeVirtualMemory(int vaddr, byte[] data, int offset, int length) {
    Lib.debug(dbgProc, "UserProcess.java:writeVirtualMemory(): Entering Function");
		return accessMemory(true, vaddr, data, offset, length);
	}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Load the executable with the specified name into this process, and
	 * prepare to pass it the specified arguments. Opens the executable, reads
	 * its header information, and copies sections and arguments into this
	 * process's virtual memory.
	 * 
	 * @param name the name of the file containing the executable.
	 * @param args the arguments to pass to the executable.
	 * @return <tt>true</tt> if the executable was successfully loaded.
	 */
	private boolean load(String name, String[] args) {
		Lib.debug(dbgProcess, "UserProcess.load(\"" + name + "\")");

		OpenFile executable = ThreadedKernel.fileSystem.open(name, false);
		if (executable == null) {
			Lib.debug(dbgProcess, "\topen failed");
			return false;
		}

		try {
			coff = new Coff(executable);
		}
		catch (EOFException e) {
			executable.close();
			Lib.debug(dbgProcess, "\tcoff load failed");
			return false;
		}

		
		
	
		// make sure the sections are contiguous and start at page 0
		numPages = 0;
		for (int s = 0; s < coff.getNumSections(); s++) {
			CoffSection section = coff.getSection(s);
			if (section.getFirstVPN() != numPages) {
				coff.close();
				Lib.debug(dbgProcess, "\tfragmented executable");
				return false;
			 }
      Lib.debug(dbgProc, "UserProcess.java:load(): numPages += " + section.getLength());
			numPages += section.getLength();
		}

		// make sure the argv array will fit in one page
		byte[][] argv = new byte[args.length][];
		int argsSize = 0;
		for (int i = 0; i < args.length; i++) {
			argv[i] = args[i].getBytes();
			// 4 bytes for argv[] pointer; then string plus one for null byte
			argsSize += 4 + argv[i].length + 1;
		}
		if (argsSize > pageSize) {
			coff.close();
			Lib.debug(dbgProcess, "\targuments too long");
			return false;
		}

		// program counter initially points at the program entry point
		initialPC = coff.getEntryPoint();
    Lib.debug(dbgProc, "UserProcess.java:load(): initialPC = " + initialPC);

		// next comes the stack; stack pointer initially points to top of it
    Lib.debug(dbgProc, "UserProcess.java:load(): numPages = " + numPages);
		numPages += stackPages;
    Lib.debug(dbgProc, "UserProcess.java:load(): numPages = " + numPages);
		initialSP = numPages * pageSize;
    Lib.debug(dbgProc, "UserProcess.java:load(): initialSP = " + initialSP);

		// and finally reserve 1 page for arguments
    Lib.debug(dbgProc, "UserProcess.java:load(): numPages = " + numPages);
		numPages++;
    Lib.debug(dbgProc, "UserProcess.java:load(): numPages = " + numPages);

		if (!loadSections())
			return false;

		// store arguments in last page
		int entryOffset = (numPages - 1) * pageSize;
		int stringOffset = entryOffset + args.length * 4;
	  Lib.debug(dbgProcess, "entryOffset = " + entryOffset);
	  Lib.debug(dbgProcess, "stringOffset = " + stringOffset);

		this.argc = args.length;
	  Lib.debug(dbgProcess, "argc = " + argc);
		this.argv = entryOffset;
	  Lib.debug(dbgProcess, "argv.length = " + argv.length);

		for (int i = 0; i < argv.length; i++) {
			byte[] stringOffsetBytes = Lib.bytesFromInt(stringOffset);
	    Lib.debug(dbgProc, "stringOffsetBytes = " + stringOffsetBytes);
			Lib.assertTrue(writeVirtualMemory(entryOffset, stringOffsetBytes) == 4);
			entryOffset += 4;
			Lib.assertTrue(writeVirtualMemory(stringOffset, argv[i]) == argv[i].length);
			stringOffset += argv[i].length;
			Lib.assertTrue(writeVirtualMemory(stringOffset, new byte[] { 0 }) == 1);
			stringOffset += 1;
		}
	  Lib.debug(dbgProcess, "exiting load");

		return true;
	}

	/**
	 * Allocates memory for this process, and loads the COFF sections into
	 * memory. If this returns successfully, the process will definitely be run
	 * (this is the last step in process initialization that can fail).
	 * 
	 * @return <tt>true</tt> if the sections were successfully loaded.
	 */
	protected boolean loadSections() {
    Lib.debug(dbgProc, "UserProcess.java:loadSections(): Entering Function");
		
		boolean intStatus = Machine.interrupt().disable();
		
    // Get physical pages for this section
    
	  if (numPages > Machine.processor().getNumPhysPages()) {
			coff.close();
			Lib.debug(dbgProcess, "\tinsufficient physical memory");
			return false;
		}

		//sectionPageCount = countSectionPages();
	  Lib.debug(dbgProcess, "sectionPageCount = " + sectionPageCount);
    
	  Lib.debug(dbgProcess, "numPages = " + numPages);
		
    // returns array of numPages many pages that are free
    int[] ppns = UserKernel.partitionPhysicalPages(numPages);
    for (int i = 0; i < ppns.length; i++)
	    Lib.debug(dbgProcess, "ppns["+i+"] = " + ppns[i]);
    int vpn;
    int ppn;
    int allocCount = 0;

		// Correct amount of physical pages
		if (ppns.length == numPages) {
      // Set translationTable to be an array of Translation Entries of 
      // length = max pages per proccess
      pageTable = new TranslationEntry[ppns.length];
      // load sections
      // coff gets set in load() before this gets executed
			for (int s = 0; s < coff.getNumSections(); s++) {
				CoffSection section = coff.getSection(s);
				Lib.debug(dbgProcess, "\tinitializing " + section.getName() + " section (" + section.getLength() + " pages)");
				for (int i = 0; i < section.getLength(); i++) {
					// map virtual pages to physical pages
					vpn = section.getFirstVPN() + i;
					ppn = ppns[vpn];
				  Lib.debug(dbgProcess, "vpn = " + vpn);
				  Lib.debug(dbgProcess, "ppn = " + ppn);
				  Lib.debug(dbgProcess, "section.isReadOnly = " + section.isReadOnly());
				
          // Store translation entry 
          pageTable[allocCount] = new TranslationEntry(vpn, ppn, true, section.isReadOnly(), false, false);
				  Lib.debug(dbgProcess, "Making translation entry in pageTable["+allocCount+"]");
				  
          if (pageTable[allocCount] != null){
				    Lib.debug(dbgProcess, "pageTable["+allocCount+"] is full");
          }
          else {
				    Lib.debug(dbgProcess, "pageTable["+allocCount+"] is null");
            return false;
          }
					
				  Lib.debug(dbgProcess, "about to load Page");
          // for now, just assume virtual addresses=physical addresses
					section.loadPage(i, ppn);
          allocCount++;
				  Lib.debug(dbgProcess, "loaded page");
				}
				Lib.debug(dbgProcess, "going to next section");
      }


			// Make entries in page table for stack
			for (int s = allocCount; s <= stackPages+allocCount; s++) {
          if (s == stackPages+allocCount)
            Lib.debug(dbgProcess, "Allocating page for arguments");
          else
            Lib.debug(dbgProcess, "Allocating page for stack");
					vpn = s;
					ppn = ppns[vpn];
				  Lib.debug(dbgProcess, "vpn = " + vpn);
				  Lib.debug(dbgProcess, "ppn = " + ppn);
          pageTable[s] = new TranslationEntry(vpn, ppn, true, false, false, false);
      }

      //Make entry for program arguments
      //pageTable[allocCount + stackPages] = new TranslationEntry(vpn, ppn, true, false, false, false);

		  Lib.debug(dbgProcess, "no more sections");
		  restoreState();

      return true;
	// No physical pages to give up. Close the coff
  }
    return true;
  }

	/**
	 * Release any resources allocated by <tt>loadSections()</tt>.
	 */
	protected void unloadSections() {
		// Release the coff resouce
		coff.close();

    zeroOutPageMemory();
		
		int[] releasePages = new int[sectionPageCount];
		// give the pages back
		for (int i = 0; i < sectionPageCount; i++) {
			Lib.debug(dbgProc, "UserProcess.java:unloadSections(): Giving Pages Back");
			// Give the physical page number back to UserKernel
			releasePages[i] = pageTable[i].ppn;
			// delete entry in pageTable
			pageTable[i] = null;
		}
	  UserKernel.reclaimPhysicalPages(releasePages);
	}

	/**
	 * Initialize the processor's registers in preparation for running the
	 * program loaded into this process. Set the PC register to point at the
	 * start function, set the stack pointer register to point at the top of the
	 * stack, set the A0 and A1 registers to argc and argv, respectively, and
	 * initialize all other registers to 0.
	 */
	public void initRegisters() {
    Lib.debug(dbgProc, "UserProcess.java:initRegisters()");
		Processor processor = Machine.processor();

		// by default, everything's 0
		for (int i = 0; i < processor.numUserRegisters; i++)
			processor.writeRegister(i, 0);

		// initialize PC and SP according
		processor.writeRegister(Processor.regPC, initialPC);
		processor.writeRegister(Processor.regSP, initialSP);

		// initialize the first two argument registers to argc and argv
		processor.writeRegister(Processor.regA0, argc);
		processor.writeRegister(Processor.regA1, argv);
	}

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
	
	/**
	 * Handle the halt() system call.
	 */
	private int handleHalt() {
    	Lib.debug(dbgProc, "UserProcess.java:handleHalt(): Entering Function");

		if (ID == 0) {
			Lib.debug(dbgProc, "UserProcess.java:handleHalt(): Machine.halt() called by root process");
			Machine.halt();
		}
		
		// Child process of root attempting to call handleHalt()
		Lib.debug(dbgProc, "UserProcess.java:handleHalt(): ERROR: child process called Machine.halt()");
		
		Lib.assertNotReached("Machine.halt() did not halt machine!");
		return 0;
	}
	
	/**
	 * Handle the creat() system call.
	 */
	private int handleCreate(int name) {
		Lib.debug(dbgProc, "UserProcess.java:handleCreate(): Entering Function");
		
		OpenFile newFile;
		String fileName = getString(name);
		
		// ERROR CHECK: Did user process already meet max file table size?
		if (fileManager.openFileCount == fileManager.fileTableSize) {
			Lib.debug(dbgProc, "UserProcess.java:handleCreate(): ERROR openFileCount == fileTableSize");
			return -1;
		}
		
		// ERROR CHECK: Does file already exist?
		newFile = ThreadedKernel.fileSystem.open(fileName, false);
		
		if (newFile != null) {
			Lib.debug(dbgProc, "UserProcess.java:handleCreate(): ERROR: file already exists");
			return -1;
		}
	
		// ERROR CHECK: Confirm new file is created
		newFile = ThreadedKernel.fileSystem.open(fileName, true);
		if (newFile == null) {
			Lib.debug(dbgProc, "UserProcess.java:handleCreate(): ERROR: Failed to create new file");
			return -1;
		}
		
		// Find Empty Spot in fileTable
		// At this point, fileTable will have atleast one open spot
		int i = 0;
		while (fileManager.fileTable[i] != null) { i++; }
			fileManager.fileTable[i] = newFile;
			fileManager.openFileCount++;
		
		Lib.debug(dbgProc, "UserProcess.java:handleCreate(): SUCCESS leaving Function");
		return i;
	}

	/**
	 * Terminate the current process immediately. Any open file descriptors
	 * belonging to the process are closed. Any children of the process no longer
	 * have a parent process.
	 *
	 * status is returned to the parent process as this process's exit status and
	 * can be collected using the join syscall. A process exiting normally should
	 * (but is not required to) set status to 0.
	 *
	 * exit() never returns.
	 */
	private int handleExit(int status) {
		Lib.debug(dbgProc, "UserProcess.java:handleExit(): Entering Function");
		// Terminate current process
		
		// Set all children's PIDs to 0
		while(!children.isEmpty()) {
			children.remove().PID = 0;
		}
		
		unloadSections();
		fileManager.closeAllFiles();
		if (--processCount == 0) {
			
			Kernel.kernel.terminate();
		}
		
		KThread.finish();
    
		Lib.debug(dbgProc, "UserProcess.java:handleExit(): SUCCESS Leaving Function");
		return 0;
	}

/**
 * Execute the program stored in the specified file, with the specified
 * arguments, in a new child process. The child process has a new unique
 * process ID, and starts with stdin opened as file descriptor 0, and stdout
 * opened as file descriptor 1.
 *
 * file is a null-terminated string that specifies the name of the file
 * containing the executable. Note that this string must include the ".coff"
 * extension.
 *
 * argc specifies the number of arguments to pass to the child process. This
 * number must be non-negative.
 *
 * argv is an array of pointers to null-terminated strings that represent the
 * arguments to pass to the child process. argv[0] points to the first
 * argument, and argv[argc-1] points to the last argument.
 *
 * exec() returns the child process's process ID, which can be passed to
 * join(). On error, returns -1.
 */
	private int handleExec(int file, int argc, int argv) {
		boolean intStatus = Machine.interrupt().disable();
		Lib.debug(dbgProc, "UserProcess.java:handleExec(): Entering Function");
		Lib.debug(dbgProc, "UserProcess.java:handleExec(): arg0: file = " + file);
		Lib.debug(dbgProc, "UserProcess.java:handleExec(): arg0: argc = " + argc);
		Lib.debug(dbgProc, "UserProcess.java:handleExec(): arg0: argv = " + argv);
		
		
		// filename is the name of the file to execute
    String filename = getString(file);
		Lib.debug(dbgProc, "UserProcess.java:handleExec(): filename = " + filename);
    // ERROR CHECKS: Expecting ".coff" in filename
    if (filename.indexOf(".coff") == -1) {
		  Lib.debug(dbgProc, "UserProcess.java:handleExec(): ERROR: '.coff' not found in filename");
      return -1;
    }
	 	
    // ERROR CHECK: Expecting argc >= 0
    if (argc < 0) {
		  Lib.debug(dbgProc, "UserProcess.java:handleExec(): ERROR: Negative argc");
      return -1;
    }
    
    // The arguments provided 
    byte[] argvb = new byte[argc*4];
    int[] argvi = new int[argc];
		//Lib.debug(dbgProc, "UserProcess.java:handleExec(): argv = " + argvb);
		Lib.debug(dbgProc, "UserProcess.java:handleExec(): argv length = " + argc);

    readVirtualMemory(argv,argvb,0,4*argc);
		Lib.debug(dbgProc, "UserProcess.java:handleExec(): argv0= " + argvb[0]);
		Lib.debug(dbgProc, "UserProcess.java:handleExec(): argv1= " + argvb[1]);
		Lib.debug(dbgProc, "UserProcess.java:handleExec(): argv2= " + argvb[2]);
		Lib.debug(dbgProc, "UserProcess.java:handleExec(): argv3= " + argvb[3]);

    //argvi[0] = argvb[0] & argvb[1] & argvb[2] & argvb[3];
    // Number of args 
    String[] args = new String[argc];
    int vaddr;
    for (int i=0; i<argc; i++){
      vaddr =(argvb[3]<<24)&0xff000000|
             (argvb[2]<<16)&0x00ff0000|
             (argvb[1]<< 8)&0x0000ff00|
             (argvb[0]<< 0)&0x000000ff;
		  Lib.debug(dbgProc, "UserProcess.java:handleExec(): vaddr = " + vaddr);
      args[i] = getString(vaddr);
		  Lib.debug(dbgProc, "UserProcess.java:handleExec(): args["+i+"] = " + args[i]);
    }


		// Create a child process
		UserProcess childProcess = new UserProcess();
    if (childProcess == null) {
		  Lib.debug(dbgProc, "UserProcess.java:handleExec(): ERROR: Failed to create child proccess");
      return -1;
    }

		// Set the IDs
		childProcess.PID = ID;
		Lib.debug(dbgProc, "UserProcess.java:handleExec(): Setting childProcess parent ID to: " + childProcess.PID);
		
		// Add to processes children
		children.add(childProcess);
		Lib.debug(dbgProc, "UserProcess.java:handleExec(): Adding childProcess to Parent's Children");
		
		// file must contain .coff extension
		childProcess.execute(filename, args);  

		Lib.debug(dbgProc, "UserProcess.java:handleExec(): SUCCESS Leaving Function");
		Machine.interrupt().restore(intStatus);
		return childProcess.ID;
	}

	/**
	 * Suspend execution of the current process until the child process specified
	 * by the processID argument has exited. If the child has already exited by the
	 * time of the call, returns immediately. When the current process resumes, it
	 * disowns the child process, so that join() cannot be used on that process
	 * again.
	 *
	 * processID is the process ID of the child process, returned by exec().
	 *
	 * status points to an integer where the exit status of the child process will
	 * be stored. This is the value the child passed to exit(). If the child exited
	 * because of an unhandled exception, the value stored is not defined.
	 *
	 * If the child exited normally, returns 1. If the child exited as a result of
	 * an unhandled exception, returns 0. If processID does not refer to a child
	 * process of the current process, returns -1.
	 */
	private int handleJoin(int processID, int status) {
		Lib.debug(dbgProc, "UserProcess.java:handleJoin(): Entering Function");
		
		// Flag to check if childProcess with processID exists
		boolean childProcessExists = false;
		
		// Check if processID refers to a child of current process
		ListIterator<UserProcess> listIterator = children.listIterator();
		// Initialize a new childProcess to join
		UserProcess childProcess = null;
		while (listIterator.hasNext()) {
			childProcess = listIterator.next();
			if (childProcess.ID == processID)  {
				childProcessExists = true;
				// remove the child from children
				children.remove();
				break;
			}
		}
		// No Child Process with ID of processID
		if (!childProcessExists)
			return -1;
		
		// We have the correct child process, join the process
		childProcess.thread.join();

		Lib.debug(dbgProc, "UserProcess.java:handleJoin(): SUCCESS Leaving Function");
		return 1;
	}

	/**
	 * Handle the open() system call.
	 */	
	private int handleOpen(int name) {
		Lib.debug(dbgProc, "UserProcess.java:handleOpen(): Entering Function");

		String filename = getString(name);
    
		// ERROR CHECK: Did user process already meet max file table size?
		if (fileManager.openFileCount == fileManager.fileTableSize) {
			Lib.debug(dbgProc, "UserProcess.java:handleOpen(): openFileCount == fileTableSize");
			return -1;
		}

		// ERROR CHECK: Does file exist?
		OpenFile openfile = ThreadedKernel.fileSystem.open(filename,false);
		if (openfile == null) {
			Lib.debug(dbgProc, "UserProcess.java:handleOpen(): openFile == null");
			return -1;
		}

		// Loop to find the empty spot in filetable
		int i = 0;
		while (fileManager.fileTable[i] != null && i < fileManager.fileTableSize) { i++; }	
		if ( i == fileManager.fileTableSize) {
			Lib.debug(dbgProc, "UserProcess.java:handleOpen(): openFileCount == fileTableSize");
			return -1;
    }
    
    fileManager.fileTable[i] = openfile;
    fileManager.openFileCount++;

		Lib.debug(dbgProc, "UserProcess.java:handleOpen(): SUCCESS Leaving Function");
		return i;
	}

	/**
	 * Handle the read() system call.
	 */
	private int handleRead(int fileDescriptor, int buffer, int count) {
		Lib.debug(dbgProc, "UserProcess.java:handleRead(): Entering Function");

		// To be used for container
		byte buf[] = new byte[count];
		OpenFile fileToRead = fileManager.fileTable[fileDescriptor];
		
		if (fileToRead == null) {
			return -1;
		}

		// Set for shortened name
		//int pos = fileManager.fileDescriptorLastRead[fileDescriptor];
	
		// Number of bytes read
		int numRead = fileToRead.read(buf, 0, count);

		// file-->buf
		//numRead = fileManager.fileTable[fileDescriptor].read(pos, buf, 0, count);
    
    		// Update the position of file's last read location
		//fileManager.fileDescriptorLastRead[fileDescriptor] += numRead;
		
    		// buf-->pvm
		writeVirtualMemory(buffer,buf);

		return numRead;
	}

	/**
	 * Handle the write() system call.
	 */
	private int handleWrite(int fileDescriptor, int buffer, int count) {
		Lib.debug(dbgProc, "UserProcess.java:handleWrite(): Entering Function");
		Lib.debug(dbgProc, "UserProcess.java:handleWrite(): arg0: fileDescriptor = " + fileDescriptor);
		Lib.debug(dbgProc, "UserProcess.java:handleWrite(): arg1: buffer = "+buffer);
		Lib.debug(dbgProc, "UserProcess.java:handleWrite(): arg2: count = " + count);
 
		// To be used for container
		Lib.debug(dbgProc, "UserProcess.java:handleWrite(): Alloc buf");
		byte buf[] = new byte[count];
		OpenFile fileToWrite = fileManager.fileTable[fileDescriptor];
		
		if (fileToWrite == null) {
			return -1;
		}
		
		// Set for shortened name
		Lib.debug(dbgProc, "UserProcess.java:handleWrite(): Setting pos");
		//int pos = fileManager.fileDescriptorLastRead[fileDescriptor];
		//Lib.debug(dbgProc, "UserProcess.java:handleWrite(): pos = " + pos);

		// Read process's virtual memory to buf
		// pvm-->buf
		Lib.debug(dbgProc, "UserProcess.java:handleWrite(): Readvirtualmemory");
		readVirtualMemory(buffer,buf);

		// Number of bytes written
		Lib.debug(dbgProc, "UserProcess.java:handleWrite(): Entering Function");
		int numWrite = fileToWrite.write(buf, 0, count);

		// Write buf to file
		// buf-->file
		Lib.debug(dbgProc, "UserProcess.java:handleWrite(): Entering Function");
		//numWrite = fileManager.fileTable[fileDescriptor].write(pos, buf, 0, count);

		// Update the position of file's last written location
		Lib.debug(dbgProc, "UserProcess.java:handleWrite(): Entering Function");
		//fileManager.fileDescriptorLastWrite[fileDescriptor] += numWrite;
		
		return numWrite;
	}

	/**
	 * Handle the close() system call.
	 */
	private int handleClose(int fileDescriptor) {
		Lib.debug(dbgProc, "UserProcess.java:handleClose(): Entering Function");
		
		// ERROR CHECK: fileDescriptor is between 0 and fileTabeSize, inclusive
		if (fileDescriptor < 0 || fileDescriptor >= fileManager.fileTableSize) {
			Lib.debug(dbgProc, "UserProcess.java:handleClose(): File Descriptor doesn't exist");
			return -1;
		}

		// ERROR CHECK: Check if attempting to close nonexistent file
		if (fileManager.fileTable[fileDescriptor] == null) {
			Lib.debug(dbgProc, "UserProcess.java:handleClose(): Closing file not in table");
			return -1;
		}
		fileManager.fileTable[fileDescriptor].close();
		fileManager.fileTable[fileDescriptor] = null;
		// TODO causes a null pointer exception
		//fileManager.fileDescriptorLastRead[fileDescriptor] = 0;
		//fileManager.fileDescriptorLastWrite[fileDescriptor] = 0;
		fileManager.openFileCount--;
		return 0;
	}
	/**
	 * Handle the unlink() system call.
	 */	
	private int handleUnlink(int name) {
		Lib.debug(dbgProc, "UserProcess.java:handleUnlink(): Entering Function");
		
		String fileName = getString(name);
		// ERROR CHECK: Does file exist?
 		OpenFile openfile = ThreadedKernel.fileSystem.open(fileName, false);
		if (openfile == null) {
			return -1;
		}

		// Search for index with matching filename
		int i = 0;
		while (fileManager.fileTable[i].getName() != openfile.getName() && 
			i < fileManager.fileTableSize) { i++; }

		// Success
		if (fileManager.fileTable[i].getName() == openfile.getName()) {
			fileManager.fileTable[i] = null;
			//fileManager.fileDescriptorLastRead[i] =0;
			//fileManager.fileDescriptorLastWrite[i] =0;
			fileManager.openFileCount--;
			return 0;
		}

		// ERROR: No matching filename found in filetable
		return -1;
	}

	/** Syscall definitions. **/
	private static final int syscallHalt = 0, syscallExit = 1, syscallExec = 2,
			syscallJoin = 3, syscallCreate = 4, syscallOpen = 5,
			syscallRead = 6, syscallWrite = 7, syscallClose = 8,
			syscallUnlink = 9;

	/**
	 * Handle a syscall exception. Called by <tt>handleException()</tt>. The
	 * <i>syscall</i> argument identifies which syscall the user executed:
	 * 
	 * <table>
	 * <tr>
	 * <td>syscall#</td>
	 * <td>syscall prototype</td>
	 * </tr>
	 * <tr>
	 * <td>0</td>
	 * <td><tt>void halt();</tt></td>
	 * </tr>
	 * <tr>
	 * <td>1</td>
	 * <td><tt>void exit(int status);</tt></td>
	 * </tr>
	 * <tr>
	 * <td>2</td>
	 * <td><tt>int  exec(char *name, int argc, char **argv);
	 * 								</tt></td>
	 * </tr>
	 * <tr>
	 * <td>3</td>
	 * <td><tt>int  join(int pid, int *status);</tt></td>
	 * </tr>
	 * <tr>
	 * <td>4</td>
	 * <td><tt>int  creat(char *name);</tt></td>
	 * </tr>
	 * <tr>
	 * <td>5</td>
	 * <td><tt>int  open(char *name);</tt></td>
	 * </tr>
	 * <tr>
	 * <td>6</td>
	 * <td><tt>int  read(int fd, char *buffer, int size);
	 * 								</tt></td>
	 * </tr>
	 * <tr>
	 * <td>7</td>
	 * <td><tt>int  write(int fd, char *buffer, int size);
	 * 								</tt></td>
	 * </tr>
	 * <tr>
	 * <td>8</td>
	 * <td><tt>int  close(int fd);</tt></td>
	 * </tr>
	 * <tr>
	 * <td>9</td>
	 * <td><tt>int  unlink(char *name);</tt></td>
	 * </tr>
	 * </table>
	 * 
	 * @param syscall the syscall number.
	 * @param a0 the first syscall argument.
	 * @param a1 the second syscall argument.
	 * @param a2 the third syscall argument.
	 * @param a3 the fourth syscall argument.
	 * @return the value to be returned to the user.
	 */
	public int handleSyscall(int syscall, int a0, int a1, int a2, int a3) {
		switch (syscall) {
		case syscallHalt:
			return handleHalt();
		case syscallExit:
			return handleExit(a0);
		case syscallExec:
			return handleExec(a0, a1, a2);
		case syscallJoin:
			return handleJoin(a0, a1);
		case syscallCreate:
			return handleCreate(a0);
		case syscallOpen:
			return handleOpen(a0);
		case syscallRead:
			return handleRead(a0, a1, a2);
		case syscallWrite:
			return handleWrite(a0, a1, a2);
		case syscallClose:
			return handleClose(a0);
		
		default:
			Lib.debug(dbgProcess, "Unknown syscall " + syscall);
			Lib.assertNotReached("Unknown system call!");
		}
		return 0;
	}

	/**
	 * Handle a user exception. Called by <tt>UserKernel.exceptionHandler()</tt>
	 * . The <i>cause</i> argument identifies which exception occurred; see the
	 * <tt>Processor.exceptionZZZ</tt> constants.
	 * 
	 * @param cause the user exception that occurred.
	 */
	public void handleException(int cause) {
		Processor processor = Machine.processor();

		switch (cause) {
		case Processor.exceptionSyscall:
			int result = handleSyscall(processor.readRegister(Processor.regV0),
					processor.readRegister(Processor.regA0),
					processor.readRegister(Processor.regA1),
					processor.readRegister(Processor.regA2),
					processor.readRegister(Processor.regA3));
			processor.writeRegister(Processor.regV0, result);
			processor.advancePC();
			break;

		default:
			Lib.debug(dbgProcess, "Unexpected exception: "
					+ Processor.exceptionNames[cause]);
			Lib.assertNotReached("Unexpected exception");
		}
  }

///////////////////////////////////////////////////////////////
// HELPER FUNCTIONS
///////////////////////////////////////////////////////////////

  private String getString(int vaddr) {
    return readVirtualMemoryString(vaddr, maxFileLength);
  }
  
  private int accessMemory(boolean write, int vaddr, byte[] data, int offset, int length) {
		boolean intStatus = Machine.interrupt().disable();
    Lib.debug(dbgProc, "UserProcess.java:accessMemory(): Entering Function");
    Lib.debug(dbgProc, "UserProcess.java:accessMemory(): arg0: write = " + write);
    Lib.debug(dbgProc, "UserProcess.java:accessMemory(): arg1: vaddr = " + vaddr);
    Lib.debug(dbgProc, "UserProcess.java:accessMemory(): arg2: dataLength = " + data.length);
    Lib.debug(dbgProc, "UserProcess.java:accessMemory(): arg3: offset = " + offset);
    Lib.debug(dbgProc, "UserProcess.java:accessMemory(): arg4: length = " + length);
		UserKernel.pageTableLock.acquire();
    
    Lib.debug(dbgProc, "UserProcess.java:accessMemory(): Setting page table");
	  Machine.processor().setPageTable(pageTable);	
		
    Lib.assertTrue(offset >= 0 && length >= 0
				&& offset + length <= data.length);

		byte[] memory = Machine.processor().getMemory();
    int memLength = memory.length;
    int dataLength = data.length;
    Lib.debug(dbgProc, "UserProcess.java:accessMemory(): memLength = " + memLength);
		int paddr = translateVA(vaddr, memLength);
    Lib.debug(dbgProc, "UserProcess.java:accessMemory(): paddr = " + paddr);

		// Handles corner case
		int amount = Math.min(length, memory.length - vaddr);
    Lib.debug(dbgProc, "UserProcess.java:accessMemory(): amount = " + amount);

		// Write data to memory
		if (write) {
      Lib.debug(dbgProc, "UserProcess.java:accessMemory(): Writing to memory");
			System.arraycopy(data, 0, memory, paddr, amount);
		}
		else {
      Lib.debug(dbgProc, "UserProcess.java:accessMemory(): Reading from memory");
			System.arraycopy(memory, paddr, data, 0, amount);
		}
    
		UserKernel.pageTableLock.release();

    Lib.debug(dbgProc, "UserProcess.java:accessMemory(): Exiting Function");
		Machine.interrupt().restore(intStatus);
		return amount;
  }
  

  /**
   * Loops through all coffSections
   *
   *
   *
   */
  protected int countSectionPages() {
    int totalPages = 0;
    for (int s = 0; s < coff.getNumSections(); s++) {
      CoffSection section = coff.getSection(s);
      totalPages += section.getLength();
      }
    return totalPages;
  }

  private int translateVA(int vaddr, int memLength) {
    Lib.debug(dbgProc, "UserProcess.java:translateVPN(): Entering Function");

    if (vaddr < 0 || vaddr >= memLength) {
    	Lib.debug(dbgProc, "UserProcess.java:writeVirtualMemory(): ERROR: vpn out of range");
			return -1;
    }

    int vpn = Machine.processor().pageFromAddress(vaddr);
    int offset = Machine.processor().offsetFromAddress(vaddr);
    int ppn = pageTable[vpn].ppn;
    
    
    if (ppn < 0 || ppn >= memLength) {
    	Lib.debug(dbgProc, "UserProcess.java:writeVirtualMemory(): ERROR: ppn out of range");
			return -1;
    }

    return Machine.processor().makeAddress(ppn,offset);
  }

  private int zeroOutPageMemory() {
    Lib.debug(dbgProc, "UserProcess.java:zeroOutMemory(): Entering Function");
    byte[] zero = new byte[pageSize];
    for(int i = 0; i < sectionPageCount; i++) {
      if (writeVirtualMemory(i,zero) != pageSize) {
        Lib.debug(dbgProc, "UserProcess.java:zeroOutMemory(): ERROR: zero failed to fully write");
        Lib.debug(dbgProc, "UserProcess.java:zeroOutMemory(): Exiting Function");
        return -1;
      }
    }
    Lib.debug(dbgProc, "UserProcess.java:zeroOutMemory(): Exiting Function");
    return 0;
  }




///////////////////////////////////////////////////////////////


	/** The program being run by this process. */
	protected Coff coff;

	/** This process's page table. */
	protected TranslationEntry[] pageTable;

	/** The number of contiguous pages occupied by the program. */
	protected int numPages;

	/** The number of pages in the program's stack. */
	protected final int stackPages = 8;

	private int initialPC, initialSP;

	private int argc, argv;

	private static final int pageSize = Processor.pageSize;

	private static final char dbgProcess = 'y';

///////////////////////////////////////////////////////////////

	/** Custom Debug Process flag 'y'. **/	
	private static final char dbgProc = 'o';

	/** OpenFile manager. **/
	private openFileManager fileManager;

  private String[] processArgs;

  private int sectionPageCount;

  private static int processCount = 0; 

  private static int nextID = 0; 
  
  protected int ID;
  
  public int PID;

  private LinkedList<UserProcess> children = new LinkedList<UserProcess>();
  
  private int maxFileLength = 256;
  
  /* The current thread */
  private UThread thread;

}
