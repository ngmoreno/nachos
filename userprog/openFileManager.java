package nachos.userprog;

import nachos.machine.*;
import nachos.threads.*;
import nachos.userprog.*;

/**
 * OpenFile Manager that controls a fileTable
 */
public class openFileManager {
	/**
	 * Constructor to keep track of Kernel fileTable
	 * Initializes a new fileTable and used the first and second entry for
	 * openForReading() and openForWriting()	
	 */
	public openFileManager() {
    Lib.debug(dbgProc, "openFileManager.java:openFileManager(): initializing openFileManager()");
		fileTable = new OpenFile[fileTableSize];
    Lib.debug(dbgProc, "openFileManager.java:openFileManager(): initializing openFileManager()");
		fileTable[0] = UserKernel.console.openForReading();
    Lib.debug(dbgProc, "openFileManager.java:openFileManager(): initializing openFileManager()");
		fileTable[1] = UserKernel.console.openForWriting();
    Lib.debug(dbgProc, "openFileManager.java:openFileManager(): initializing openFileManager()");
    openFileCount = 2;
    Lib.debug(dbgProc, "openFileManager.java:openFileManager(): initializing openFileManager()");
	  fileDescriptorLastRead = new int[fileTableSize];
    Lib.debug(dbgProc, "openFileManager.java:openFileManager(): initializing openFileManager()");
	  fileDescriptorLastWrite = new int[fileTableSize];
    Lib.debug(dbgProc, "openFileManager.java:openFileManager(): initializing openFileManager()");
    for (int i = 0; i < fileTableSize; i++){
    Lib.debug(dbgProc, "openFileManager.java:openFileManager():looping");
	    fileDescriptorLastRead[i]  = 0;
	    fileDescriptorLastWrite[i] = 0;
    }
    Lib.debug(dbgProc, "openFileManager.java:openFileManager(): done init ");
	}
	
	public void closeAllFiles() {
		for (int i = 0; i < fileTableSize; i++) {
			if (fileTable[i] != null)
				fileTable[i].close();
		}
	}

  private boolean checkFileDescriptor(int fileDescriptor) {
		if (fileDescriptor < 0 || fileDescriptor >= fileTableSize) {
			Lib.debug(dbgProc, "openFileManager.java:checkFileDescriptor(): File Descriptor doesn't exist");
			return false;
		}
    return true;
  }
	private boolean checkSpotFull(int fileDescriptor) {	
    if (fileTable[fileDescriptor] == null) {
			Lib.debug(dbgProc, "openFileManager.java:checkFileExist(): spot is full");
			return true;
		}
    else {
      Lib.debug(dbgProc, "openFileManager.java:checkFileExist(): spot is empty");
      return false;
    }
  }
		
	private boolean checkMax() {	
    // ERROR CHECK: Did user process already meet max file table size?
		if (openFileCount == fileTableSize) {
			Lib.debug(dbgProc, "openFileManager.java:max(): ");
			return true;
    }
    return false;
  }
		
  private boolean checkFileExist(String filename) {
		// ERROR CHECK: Does file exist?
		OpenFile openfile = ThreadedKernel.fileSystem.open(filename,false);
		return openfile == null;
  }

  // Returns first empty spot in fileTable
  private int getEmptyFD() {
		// Loop to find the empty spot in filetable
    for (int i = 0; i < fileTableSize;i++) {
      if (!checkSpotFull(i)) {
        return i;
      }
    }
    return -1;
  }

  private OpenFile open(String filename) {
		OpenFile openfile = ThreadedKernel.fileSystem.open(filename,false);
    return openfile;
  }
  

  public int openFile(String filename) {
    if (!checkMax() && checkFileExist(filename)) {
      int fd = getEmptyFD();
      if ( checkFileDescriptor(fd) && !checkSpotFull(fd)) {
        fileTable[fd] = open(filename);
        openFileCount++;
        return fd;
      }
    }
    return -1;
  }

  public boolean closeFile(int fd) {
    if (checkFileDescriptor(fd) && checkSpotFull(fd)) {
      fileTable[fd].close();
      openFileCount--;
      return true;
    }
    return false;
  }
	
	// Probably best to abstract these away into getter/setter methods
	private static final char dbgProc = 'f';
	public final int fileTableSize = 16;

	public int openFileCount = 0;

	public OpenFile[] fileTable;

	public int[] fileDescriptorLastRead;

	public int[] fileDescriptorLastWrite;
}

