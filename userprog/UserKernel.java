package nachos.userprog;

import java.util.LinkedList;

import nachos.machine.*;
import nachos.threads.*;
import nachos.userprog.*;

/**
 * A kernel that can support multiple user processes.
 */
public class UserKernel extends ThreadedKernel {
	/**
	 * Allocate a new user kernel.
	 */
	public UserKernel() {
		super();
	}

	/**
	 * Initialize this kernel. Creates a synchronized console and sets the
	 * processor's exception handler.
	 */
	public void initialize(String[] args) {
		super.initialize(args);

		// Maintain a global reference to the synchornization console
		console = new SynchConsole(Machine.console());
		
		// Maintain a reference to the page table lock
		pageTableLock = new Lock();

		// Maintain a reference to all, free physical pages
		freePhysicalPages = new LinkedList<Integer>();
		int maxPageCount = Machine.processor().getNumPhysPages();
		for (int i = 0; i < maxPageCount; i++)
			freePhysicalPages.add(i);

		Machine.processor().setExceptionHandler(new Runnable() {
	 		public void run() {
				exceptionHandler();
			}
		});
	}
		
	private static final int syscallHalt = 0, syscallExit = 1, syscallExec = 2, 
        	syscallJoin = 3, syscallCreate = 4, syscallOpen = 5,
        	syscallRead = 6, syscallWrite = 7, syscallClose = 8,
        	syscallUnlink = 9;


/** Added tester method. **/
	private int test(int syscall,int a,int b,int c,int d,String str,int ret) {
		System.out.println(str);

		int result = testProcess.handleSyscall(syscall,a,b,c,d);
		System.out.printf("result = %d --> ", result);
		if (result == ret)
			System.out.println("PASS\n");
		else
			System.out.printf("FAIL\nExpected: %d\n\n", ret);

		return result;
	}
/** End Added tester method. **/	

	/**
	 * Test the console device.
	 */
	public void selfTest() {
		super.selfTest();

		System.out.println("Testing the console device. Typed characters");
		System.out.println("will be echoed until q is typed.");

		char c;

		do {
			c = (char) console.readByte(true);
			console.writeByte(c);
		} while (c != 'q');

		System.out.println("");


	/** Added for testing. **/
		int normal = 0;
		int error = -1;
		int filename = 1234;
		int fd = 2;
		int fd_null  = 30;
		String teststr;
		int expect;

    boolean testvar = false;
    if (testvar) {
    
    // A Process to test syscall with
	  testProcess = UserProcess.newUserProcess();
		String shellProgram = Machine.getShellProgramName();
		Lib.assertTrue(testProcess.execute(shellProgram, new String[] {}));

		System.out.println("Starting testing");
    
		// TEST #1.0: Test create new file that doesn't already exist
		expect = fd;
		teststr = "Test create nonexistent file";
		int created_fd = test(syscallCreate, filename, 0,0,0,teststr,expect);
		System.out.printf("%s", created_fd);    
		// ERROR TEST #1.1: Test create new file that already exists
		expect = error;
		teststr = "Test create existing file";
		test(syscallCreate, filename, 0,0,0,teststr,expect);

		// TEST #2.0: Test closing a file that was opened
		expect = normal;
		teststr = "Test closing opened file";
		test(syscallClose, created_fd, 0,0,0,teststr,expect);

		// ERROR TEST #2.1: Test closing a file that hasn't been opened
		expect = error;
		teststr = "Test closing unopened file";
		test(syscallClose, fd+1, 0,0,0,teststr,expect);

		// ERROR TEST #2.2: Test closing a file that hasn't been opened
		expect = error;
		teststr = "Test closing nonexistent file";
		test(syscallClose, 1001, 0,0,0,teststr,expect);

		// ERROR TEST #3.0: Test opening a file that doesn't already exist
		expect = error;
    		teststr = "Test opening nonexistent file";
		test(syscallOpen, filename+1, 0,0,0,teststr,expect);
    
		// TEST #3.1: Test opening a file that already exists
		expect = fd;
		teststr = "Test opening existing file";
		testProcess.handleSyscall(syscallCreate,filename,0,0,0);
		fd = test(syscallOpen, filename, 0,0,0, teststr,expect);
		testProcess.handleSyscall(syscallClose,fd,0,0,0);

		//process.handleSyscall(syscallExit,
		//process.handleSyscall(syscallExec,
		//process.handleSyscall(syscallJoin,
		System.out.println("End testing");

	/** End Testing. **/

    }
	}

	/**
	 * Returns the current process.
	 * 
	 * @return the current process, or <tt>null</tt> if no process is current.
	 */
	public static UserProcess currentProcess() {
		if (!(KThread.currentThread() instanceof UThread))
			return null;

		return ((UThread) KThread.currentThread()).process;
	}

	/**
	 * The exception handler. This handler is called by the processor whenever a
	 * user instruction causes a processor exception.
	 * 
	 * <p>
	 * When the exception handler is invoked, interrupts are enabled, and the
	 * processor's cause register contains an integer identifying the cause of
	 * the exception (see the <tt>exceptionZZZ</tt> constants in the
	 * <tt>Processor</tt> class). If the exception involves a bad virtual
	 * address (e.g. page fault, TLB miss, read-only, bus error, or address
	 * error), the processor's BadVAddr register identifies the virtual address
	 * that caused the exception.
	 */
	public void exceptionHandler() {
		Lib.assertTrue(KThread.currentThread() instanceof UThread);

		UserProcess process = ((UThread) KThread.currentThread()).process;
		int cause = Machine.processor().readRegister(Processor.regCause);
		process.handleException(cause);
	}

	/**
	 * Start running user programs, by creating a process and running a shell
	 * program in it. The name of the shell program it must run is returned by
	 * <tt>Machine.getShellProgramName()</tt>.
	 * 
	 * @see nachos.machine.Machine#getShellProgramName
	 */
	public void run() {
		super.run();

		UserProcess process = UserProcess.newUserProcess();

		String shellProgram = Machine.getShellProgramName();
		Lib.assertTrue(process.execute(shellProgram, new String[] {}));

		KThread.currentThread().finish();
	}

	/**
	 * Terminate this kernel. Never returns.
	 */
	public void terminate() {
		super.terminate();
	}

////////////////////////////////////////////////////////////////////////////////
// HELPER FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

	/** 
	 * Details- Allocate given number of physical pages for process.
   *
   * author: Eric & Nathan
   *
	 * public - UserProcess needs access in loadSections()
   * static - Easyaccess
   * arg1   - int for denoting how pages to allocate
   * return - int[] storing indexes for ppns
	 */
	public static int[] partitionPhysicalPages(int numPages) {		
		boolean intStatus = Machine.interrupt().disable();
		Lib.debug(dbgProc, "UserKernel.java:partitionPhysicalPages(): Entering Function.");
		pageTableLock.acquire();
		
		
		// Check if we have enough pages to allocate. If so, allocate 
		// a new array of pageSize and remove from freePhysicalPages
		if (freePhysicalPages.size() > numPages) {
			Lib.debug(dbgProc, "UserKernel.java:partitionPhysicalPages(): freePhysicalPages > pageSize");
			int[] ppns  = new int[numPages];
			for (int i = 0; i < numPages; i++)
				ppns[i] = freePhysicalPages.remove();
			pageTableLock.release();
			Lib.debug(dbgProc, "UserKernel.java:partitionPhysicalPages(): Leaving Function.");
		  Machine.interrupt().restore(intStatus);
			return ppns;
		}
		// Not enough pages to release
		else {
			Lib.debug(dbgProc, "UserKernel.java:partitionPhysicalPages(): Not Enough freePhyscialPages");
			pageTableLock.release();
			Lib.debug(dbgProc, "UserKernel.java:partitionPhysicalPages(): Leaving Function.");
		  Machine.interrupt().restore(intStatus);
			return null;
		}
	}


	/**
	 * Reclaim a physical page.
	 * Reclaims a physical page(s) from a process
	 */
	public static void reclaimPhysicalPages(int[] pages) {
		boolean intStatus = Machine.interrupt().disable();
		Lib.debug(dbgProc, "UserKernel.java:reclaimPhysicalPages(): Entering Function.");
		pageTableLock.acquire();
		
		// Loop through pages and add them to freePhysicalPages
		for (int i = 0; i < pages.length; i++)
			freePhysicalPages.add(pages[i]);
		
		pageTableLock.release();
		Lib.debug(dbgProc, "UserKernel.java:reclaimPhysicalPages(): Leaving Function.");
		Machine.interrupt().restore(intStatus);
	}

	/** Globally accessible reference to the synchronized console. */
	public static SynchConsole console;

	// dummy variables to make javac smarter
	private static Coff dummy1 = null;	

	/** Custom Debug Process flag 'y'. **/	
	private static final char dbgProc = 'y';
	
	/** Create a new lock. **/
	public static Lock pageTableLock;

	/** Create a global linkedlist of free physical pages. **/
	public static LinkedList<Integer> freePhysicalPages;

  /** Process used for testing purposes */
  private static UserProcess testProcess;
}
