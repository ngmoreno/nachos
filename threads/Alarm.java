package nachos.threads;

import nachos.machine.*;

// Used for implementing timerInterrupt() and waitUntil()
import java.util.*;

/**
 * Uses the hardware timer to provide preemption, and to allow threads to sleep
 * until a certain time.
 */
public class Alarm {
	/**
	 * Allocate a new Alarm. Set the machine's timer interrupt handler to this
	 * alarm's callback.
	 * 
	 * <p>
	 * <b>Note</b>: Nachos will not function correctly with more than one alarm.
	 */
	public Alarm() {
    
		Machine.timer().setInterruptHandler(new Runnable() {
			public void run() {
				timerInterrupt();
			}
		});
	}

	/**
	 * The timer interrupt handler. This is called by the machine's timer
	 * periodically (approximately every 500 clock ticks). Causes the current
	 * thread to yield, forcing a context switch if there is another thread that
	 * should be run.
	 */
	public void timerInterrupt() {
		boolean intStatus = Machine.interrupt().disable();
   
		Lib.debug(dbgAlarm, "Alarm.timerInterrupt() \t\t\t<<< TIMER INTERRUPT >>>");

    // Iterate through vectors
    for (int i = 0; i < waitVector.size(); i++) {
      if ((long)waketimeVector.get(i) <= Machine.timer().getTime()) {
        // Setting state of thread to ready and remove the index from vectors
	      waitVector.remove(i).ready();
	      waketimeVector.remove(i);
      }
    }
    // Cause the current thread to yield, forcing a context switch
    // if there's another thread that should be run.
    KThread.currentThread().yield();
		Machine.interrupt().restore(intStatus);
  }

	/**
	 * Put the current thread to sleep for at least <i>x</i> ticks, waking it up
	 * in the timer interrupt handler. The thread must be woken up (placed in
	 * the scheduler ready set) during the first timer interrupt where
	 * 
	 * <p>
	 * <blockquote> (current time) >= (WaitUntil called time)+(x) </blockquote>
	 * 
	 * @param x the minimum number of clock ticks to wait.
	 * 
	 * @see nachos.machine.Timer#getTime()
	 */
	public void waitUntil(long x) {
		boolean intStatus = Machine.interrupt().disable();
	
    // Adds waketime to vector
    waketimeVector.add(Machine.timer().getTime() + x);
    
    // Store currentThread in vector
    waitVector.add(KThread.currentThread());
    // Put currentThread to sleep
    KThread.currentThread().sleep();
    Machine.interrupt().restore(intStatus);
	}

  // Stores threads that have executed waitUntil
  private static Vector<KThread> waitVector = new Vector();
  private static Vector waketimeVector = new Vector();
	private static final char dbgAlarm = 'a';

  public static void selfTest() {
    KThread t1 = new KThread(new Runnable() {
      public void run() {
        long time1 = Machine.timer().getTime();
        int waitTime = 10000;
        System.out.println("Thread calling wait at time:" + time1);
        ThreadedKernel.alarm.waitUntil(waitTime);
        System.out.println("Thread woken up after:" + (Machine.timer().getTime() - time1));
        Lib.assertTrue((Machine.timer().getTime() - time1) >= waitTime, " thread woke up too early.");

      }
    });
    t1.setName("T1");
    t1.fork();
    t1.join();
  }

}
