package nachos.threads;

import nachos.machine.*;

/**
 * A <i>communicator</i> allows threads to synchronously exchange 32-bit
 * messages. Multiple threads can be waiting to <i>speak</i>, and multiple
 * threads can be waiting to <i>listen</i>. But there should never be a time
 * when both a speaker and a listener are waiting, because the two threads can
 * be paired off at this point.
 */
public class Communicator {
	/**
	 * Allocate a new communicator.
	 */
	public Communicator() {
    //lock = new Lock();
    //cv = new Condition2;

	}

	/**
	 * Wait for a thread to listen through this communicator, and then transfer
	 * <i>word</i> to the listener.
	 * 
	 * <p>
	 * Does not return until this thread is paired up with a listening thread.
	 * Exactly one listener should receive <i>word</i>.
	 * 
	 * @param word the integer to transfer.
	 */

  // A PRODUCER WILL ONLY FILL THE BUFFER IF A SLEEPING CONSUMER EXISTS TO READ
  // A PRODUCER WILL ONLY AWAKEN A CONSUMER IF IT HAS FILLED THE BUFFER
  // A CONSUMER WILL ONLY BE AWAKE IF A PRODUCER HAS WOKEN IT 
  // OR IT HAS JUST BEGUN RUNNING
	public void speak(int word) {
    
    // lock necessary for protecting the buffer 
    lock.acquire();
   
		boolean intStatus = Machine.interrupt().disable();

    awakeProd++;
    
    // If buffer hasn't been read yet by a Consumer
    Lib.debug(dbgCom, KThread.currentThread().getName() + " is checking buf");
    while( buf != null || sleepingCons == 0 ) {
      
      // ---- BEGIN ERROR CASES ------------------------------------------------
      // NOTE: Buffer shouldn't be filled if no Consumers are available to read
      
      // ERROR CASE: Buffer was filled when no Consumer was available to read it
      if (buf != null && awakeCons == 0 && sleepingCons == 0) {
        Lib.debug(dbgCom,"ERROR 141: A Producer filled buffer with no Consumer to read the buffer");
      }
      // ERROR CASE: A Consumer died before being able to read buffer 
      if (buf != null && awakeCons == 0 && sleepingCons > 0) {
        Lib.debug(dbgCom, "ERROR 142: Something wrong happened with the awoken Consumer");
      }
      // ERROR CASE: This isn't possible because a consumer is only ever
      // awakened to read the buffer. If the buffer is null and a Consumer is
      // awake, this means a write failure had to occur.
      if (buf == null && awakeCons > 0) 
        Lib.debug(dbgCom,"WEIRD CASE: An instantiated Consumer read the buffer Another Producer awoke a listener  ");
      // ---- END ERROR CASES --------------------------------------------------

      // CASE: A Consumer doesn't exist to read the buffer
      if (buf == null && awakeCons == 0 && sleepingCons == 0) 
        Lib.debug(dbgCom, "Communicator.speak(): " +KThread.currentThread().getName() + " sees no consumers exist to read the buffer");
     
      // CASE: This Producer was scheduled to run before Consumer could read buffer
      if (buf != null && awakeCons > 0) 
        Lib.debug(dbgCom, "Communicator.speak(): " +KThread.currentThread().getName() + " sees awake Consumer that will soon read buffer");
      
      // Producer goes back to sleep
      Lib.debug(dbgCom, KThread.currentThread().getName() + " is going to sleep");
      awakeProd--;
      sleepingProd++;
      CVProd.sleep();
      
      // Producer starts running and checks conditions again
      Lib.debug(dbgCom, KThread.currentThread().getName() + " is running");
      
      // If buffer is empty and there is a sleeping Consumer: continue
    }

    // Write into buffer
    Lib.debug(dbgCom, KThread.currentThread().getName() + " is writing " + word + " into buf");
    buf = word; 
    
    // Wake the Consumer to consume the buffer
    Lib.debug(dbgCom, KThread.currentThread().getName() + " is waking a consumer");
    CVCnsmr.wake();
    sleepingCons--;
    awakeCons++;
    
    // Put the Producer to sleep to wait for Consumer Confirmation
    Lib.debug(dbgCom, KThread.currentThread().getName() + " waiting to confirm message recieved");
    awakeProd--;
    CVProdSent.sleep();
    if (read == 1) {
      Lib.debug(dbgCom, KThread.currentThread().getName() + " confirmed message recieved");
      read = 0;
    }
    
    // Wake another producer to fill the buffer
    if (awakeProd == 0 && sleepingProd > 0 && sleepingCons > 0) {
      Lib.debug(dbgCom, KThread.currentThread().getName() + " is waking the next Producer");
      CVProd.wake();
      sleepingProd--;
      awakeProd++;
    }

    lock.release();

		Machine.interrupt().restore(intStatus);

	}

	/**
	 * Wait for a thread to speak through this communicator, and then return the
	 * <i>word</i> that thread passed to <tt>speak()</tt>.
	 * 
	 * @return the integer transferred.
	 */

  // A CONSUMER WILL ONLY CONSUME IF THE BUFFER IS FULL
  // IF BUFFER ISN'T FULL, CONSUMER GOES TO SLEEP
  // IF CONSUMER IS WOKEN UP, BUFFER IS FULL
	public int listen() {

    // lock necessary for protecting the buffer 
    lock.acquire();
	  
    boolean intStatus = Machine.interrupt().disable();
    
    // To Producers, this Consumer doesn't exist before this line
    int instantiatedCons = 1;
    
    // If buffer hasn't been filled yet by a Producer
    Lib.debug(dbgCom, KThread.currentThread().getName() + " is going to start checking buffer");
    while(buf == null || (buf != null && instantiatedCons == 1)) { 
      
      // Buffer is empty
      if (buf == null)
        Lib.debug(dbgCom, "Communicator.listen(): " +KThread.currentThread().getName()+ " says buffer is empty");

      // CASE: Consumer was called after Producer
      if (awakeProd == 0 && sleepingProd > 0) {
        Lib.debug(dbgCom, "Communicator.listen(): " +KThread.currentThread().getName()+ " is attempting to wake a producer to fill the buffer");
        CVProd.wake();
        awakeProd++;
        sleepingProd--;
      }
      // CASE: Consumer was called before Producer
      else if (awakeProd == 0 && sleepingProd == 0) 
        Lib.debug(dbgCom, "Communicator.listen(): " +KThread.currentThread().getName() + " sees no producers exist to fill the buffer");
      // CASE: This Consumer was instantiated after Producer was woken by other Consumer
      else if (awakeProd > 0) 
        Lib.debug(dbgCom, "Communicator.listen(): " +KThread.currentThread().getName() + " sees a producer is already awake");
      // CASE: Buffer is full, but it's meant for another Consumer
      if (buf != null && instantiatedCons == 1) 
        Lib.debug(dbgCom, "Communicator.listen(): " +KThread.currentThread().getName() + " sees a buffer it wasn't told to read");

      // Consumer wasn't told to read buffer yet so Consumer goes to sleep
      Lib.debug(dbgCom, "Communicator.listen(): " +KThread.currentThread().getName() + " is going to sleep");
      awakeCons--;
      sleepingCons++;
      CVCnsmr.sleep();
      // Consumer was awoken by Producer to read buffer
      Lib.debug(dbgCom, "Communicator.listen(): " +KThread.currentThread().getName() + " was awoken by Producer so buffer must be full");
      Lib.debug(dbgCom, "Communicator.listen(): " +KThread.currentThread().getName() + " is going to double check the buffer");
      instantiatedCons = 0;
    }
    
    // Read buffer
    int output = buf;
    Lib.debug(dbgCom, "Communicator.listen(): " +KThread.currentThread().getName() + " is reading " + output + " from buf");

    // Set buffer to null
    buf = null;
    read = 1;
   
    // Wake the Producer that filled the buffer
    Lib.debug(dbgCom, "Communicator.listen(): " +KThread.currentThread().getName() + " is waking the tx-producer");
    CVProdSent.wake();

    // release the lock so other producers/consumers can access buffer
    lock.release();

    awakeCons--;
		Machine.interrupt().restore(intStatus);
   
		return output;
	}

	private static final char dbgCom = 's';
  private static Lock lock = new Lock();
  private static Condition2 CVProd = new Condition2(lock);
  private static Condition2 CVProdSent = new Condition2(lock);
  private static Condition2 CVCnsmr = new Condition2(lock);
  private static Integer buf = null;
  private int sleepingCons = 0;
  private int sleepingProd = 0;
  private int awakeProd = 0;
  private int awakeCons = 0;
  private int read = 0;

  public static void selfTest() {
    final Communicator com = new Communicator();
    final long times[] = new long[4];
    final int words[] = new int[2];
   
    KThread speaker1 = new KThread( new Runnable() {
      public void run() {
        //while(true) {
          com.speak(4);
          times[0] = Machine.timer().getTime();
          Lib.debug(dbgCom, KThread.currentThread().getName() + " times[0] logged time: " + times[0]);
        //}
      }
    });
    speaker1.setName("S1");
    KThread speaker2 = new KThread( new Runnable() {
      public void run() {
       // while(true) {
          com.speak(7);
          times[1] = Machine.timer().getTime();
          Lib.debug(dbgCom, KThread.currentThread().getName() + " times[1] logged time: " + times[1]);
       // }
      }
    });
    speaker2.setName("S2");
    
    KThread listener1 = new KThread( new Runnable() {
      public void run() {
        //while(true) {
          words[0] = com.listen();
          times[2] = Machine.timer().getTime();
          Lib.debug(dbgCom, KThread.currentThread().getName() + " times[2] logged time: " + times[2]);
        //}
      }
    });
    listener1.setName("L1");
    KThread listener2 = new KThread( new Runnable() {
      public void run() {
        //while(true) {
          words[1] = com.listen();
          times[3] = Machine.timer().getTime();
          Lib.debug(dbgCom, KThread.currentThread().getName() + " times[3] logged time: " + times[3]);
        //}
      }
    });
    listener2.setName("L2");
    
    speaker1.fork(); 
    speaker2.fork(); 
    listener1.fork(); 
    listener2.fork();
    
    speaker1.join(); 
    listener1.join(); 
    listener2.join();
    speaker2.join(); 
    

    Lib.assertTrue(words[0] == 4, "Didn't listen back spoken word.");
    Lib.assertTrue(words[1] == 7, "Didn't listen back spoken word.");
    Lib.assertTrue(times[0] > times[2], "speak returned before listen.");
    Lib.assertTrue(times[1] > times[3], "speak returned before listen.");

  }
}
