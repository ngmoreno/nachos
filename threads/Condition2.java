package nachos.threads;

import nachos.machine.*;
// Used for implementing timerInterrupt() and waitUntil()
import java.util.*;

/**
 * An implementation of condition variables that disables interrupt()s for
 * synchronization.
 * 
 * <p>
 * You must implement this.
 * 
 * @see nachos.threads.Condition
 */
public class Condition2 {
	/**
	 * Allocate a new condition variable.
	 * 
	 * @param conditionLock the lock associated with this condition variable.
	 * The current thread must hold this lock whenever it uses <tt>sleep()</tt>,
	 * <tt>wake()</tt>, or <tt>wakeAll()</tt>.
	 */
	public Condition2(Lock conditionLock) {
    this.conditionLock = conditionLock;
	}

	/**
	 * Atomically release the associated lock and go to sleep on this condition
	 * variable until another thread wakes it using <tt>wake()</tt>. The current
	 * thread must hold the associated lock. The thread will automatically
	 * reacquire the lock before <tt>sleep()</tt> returns.
	 */
	public void sleep() {
    Lib.debug(dbgCond, "Condition2.sleep(): Thread is entering function: " + KThread.currentThread().getName());
    //if (!conditionLock.isHeldByCurrentThread())
    //  conditionLock.acquire();
    Lib.assertTrue(conditionLock.isHeldByCurrentThread());

    // Disable Interrupt Timer
    boolean intStatus = Machine.interrupt().disable();
    
    // Release Lock
    conditionLock.release();
    
    // Store Current Thread in Queue
    KThread currentThread = KThread.currentThread();
    waitQueue.waitForAccess(currentThread); 
    
    // Put Current Thread to Sleep
    Lib.debug(dbgCond, "Condition2.sleep(): Thread is waiting: " + currentThread.getName());
    currentThread.sleep();
    Lib.debug(dbgCond, "Condition2.sleep(): Thread is running: " + currentThread.getName());
    
    // Enable Interrupt Timer
    Machine.interrupt().restore(intStatus);
    
    // Acquire Lock
    conditionLock.acquire();
	}

	/**
	 * Wake up at most one thread sleeping on this condition variable. The
	 * current thread must hold the associated lock.
	 */
	public void wake() {
    Lib.assertTrue(conditionLock.isHeldByCurrentThread());
    boolean intStatus = Machine.interrupt().disable();
		
    // Get next thread in queue and set its state to the ready state
    KThread nextThread;
    if ( (nextThread = waitQueue.nextThread()) != null) {
      nextThread.ready();
      Lib.debug(dbgCond, "Condition2.wake(): Thread is readied: " + nextThread.getName());
    }
    else {
      Lib.debug(dbgCond, "Condition2.wake(): No threads available to ready");
    }
    
    Machine.interrupt().restore(intStatus);
	}

	/**
	 * Wake up all threads sleeping on this condition variable. The current
	 * thread must hold the associated lock.
	 */
	public void wakeAll() {
    Lib.assertTrue(conditionLock.isHeldByCurrentThread());
    boolean intStatus = Machine.interrupt().disable();
    
    // For all items in blockQueue, wake them all
    KThread nextThread;
    while( (nextThread = waitQueue.nextThread()) != null) {
      nextThread.ready();
      Lib.debug(dbgCond, "Condition2.wakeAll(): Thread is readied: " + nextThread.getName());
    }
    Lib.debug(dbgCond, "Condition2.wakeAll(): No threads available to ready");
  
    Machine.interrupt().restore(intStatus);
	}

	private Lock conditionLock;

  // blockQueue stores the threads in block state over condition variable
	private ThreadQueue waitQueue = ThreadedKernel.scheduler.newThreadQueue(false);

	private static final char dbgCond = 'c';
  
  public static void selfTest() {
    final Lock lock = new Lock();
    // final condition empty = new Condition(lock);
    final Condition2 empty = new Condition2(lock);
    final LinkedList<Integer> list = new LinkedList<>();
    Lib.debug(dbgCond, "Initializing Consumer");
    KThread consumer = new KThread(new Runnable() {
      public void run() {
        System.out.println("before");
        lock.isHeldByCurrentThread();
        System.out.println("after");
        lock.acquire();
        System.out.println("before");
        lock.isHeldByCurrentThread();
        System.out.println("after");
        while(list.isEmpty()){
          empty.sleep();
        }
        Lib.assertTrue(list.size() == 5, "List should have 5 values.");
        while(!list.isEmpty()) {
          System.out.println("Removed " + list.removeFirst());
        }
        lock.release();
      }
    });

    Lib.debug(dbgCond, "Initializing Producer");
    KThread producer = new KThread(new Runnable() {
      public void run() {
        lock.acquire();
        Lib.assertTrue(lock.isHeldByCurrentThread());
        for (int i = 0; i < 5; i++) {
          list.add(i);
          System.out.println("Added " + i);
        }
        empty.wake();
        lock.release();
      }
    });

    //System.out.println("");
    consumer.setName("Consumer");
    producer.setName("Producer");
    consumer.fork();
    producer.fork();
    consumer.join();
    producer.join();
  }

}
